// Colour maps according to percentage of yes vote
var divColour = function(yes_score) {
  var yes_scale = chroma.scale("Blues").domain([0.3, 0.8]);
  var no_scale = chroma.scale("Purples").domain([0.8, 0.3]);
  if (yes_score < 0.5) {
    return no_scale(yes_score).hex();
  } else {
    return yes_scale(yes_score).hex()
  }
};
