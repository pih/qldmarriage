// Init map
var map = L.map('map').setView([-20.1392784, 146.3292991], 5.5);
map.spin(true);
// initialise empty vars for controls later
var info; var profile;



function init_style(feature) {
    return {
      fillColor: divColour(feature.properties.yes_pc),
        weight: 1,
        opacity: 1,
        color: "#efefef",
        //dashArray: '3',
        fillOpacity: 0.6
    };
}

// Function to highlight polygons on mouseover

function highlightFeature(e) {
  var layer = e.target;

  layer.setStyle({
    weight: 3,
    color: '#666',
    dashArray: '',
    fillOpacity: 0.8
  });

  if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
    layer.bringToFront();
  }

  info.update(layer.feature.properties);
  $('.dropdown').val(layer.feature.properties.CED_NAME16);
}

// When mouseoff, reset style
function resetHighlight(e) {
    districts.resetStyle(e.target);
}

// When click, zoom to the div
function zoomToFeature(e) {
  var layer = e.target;
  info.update(layer.feature.properties);
  map.fitBounds(layer.getBounds());
  $('.dropdown').val(layer.feature.properties.CED_NAME16);
};

// Bring them together
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature,
    });
  layer._leaflet_id = feature.properties.CED_NAME16;
}

// add division polygons from QLD data
var districts = new L.GeoJSON.AJAX("data/qld_survey_results.geojson", {
  style: init_style,
  onEachFeature: onEachFeature
}).addTo(map);
districts.on("data:loaded", function() {

  var Divlist = function () {
    var divs = Object.keys(map._layers).splice(3);
    var opts = "";
    for (i = 0; i < divs.length; i++) {
      opts += "<option value='" + divs[i] + "'>" + divs[i] + "</option>"
    };
    return opts
  }
  // Left-hand info, with text stats on mouseover
  info = L.control({position: 'topright'});
  info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info profile'); // create a div with a class "info"
    this._div.innerHTML = '<p><strong>Division profile</strong></p>'
    this._div.firstChild.onmousedown = this._div.firstChild.ondblclick = L.DomEvent.stopPropagation;
    this._select = L.DomUtil.create('select', 'dropdown', this._div);
    this._select.innerHTML = Divlist();
    this._profile = L.DomUtil.create('div', '', this._div); // create a div with a class "profile"

    this.update();
    return this._div;
  };
  // define contents of update
  info.update = function (props) {
    // define function for the contents of info control;
    // function is preferred for allowing for function calls
    var infoContents = function() {
      var content = "";
      if (props.yes_nr > props.no_nr) {
        content += '<p>' +
          "<strong>YES</strong> votes: <span class='winner'> " + numberWithCommas(Math.round(props.yes_nr)) +
          " (" + Math.round(props.yes_pc * 1000) / 10 + "%)</span><br/>" +
          "<strong>NO</strong> votes: " + numberWithCommas(Math.round(props.no_nr)) +
          " (" + Math.round(props.no_pc * 1000) / 10 + "%)"
      } else {
      content += '<p>' +
        "<strong>YES</strong> votes: " + numberWithCommas(Math.round(props.yes_nr)) +
        " (" + Math.round(props.yes_pc * 1000) / 10 + "%)<br/>" +
        "<strong>NO</strong> votes: <span class='winner'>" + numberWithCommas(Math.round(props.no_nr)) +
          " (" + Math.round(props.no_pc * 1000) / 10 + "%)</span>"
      }

      content += "<br/>Turnout: " + Math.round(props.turnout_pc * 1000) / 10 + "%<br/>" +
                 "Sitting party: " + props.party +"</p>"

      return content
    };
    this._profile.innerHTML = (props ? infoContents() : '<p>Hover over an electoral division, or select from the dropdown menu above.</p>');
  };
  info.addTo(map);

  var legend = L.control({position: 'bottomright'});

  legend.onAdd = function (map) {
    var div = L.DomUtil.create('div', 'info legend'),
      grades = [0.4, 0.5, 0.6, 0.7],
      labels = [];
    // loop through our density intervals and generate a label with a colored square for each interval
		div.innerHTML = '<strong>% YES vote</strong><br/>'
    for (var i = 0; i < grades.length; i++) {
      div.innerHTML +=
        '<i style="background:' + divColour(grades[i] + 0.01) + '"></i> ' +
        grades[i] * 100 + "%" + (grades[i + 1] ? '&ndash;' + grades[i + 1] * 100 + '%<br>' : '+');
    }

    return div;
  };
  legend.addTo(map);

  $('.dropdown').change(function() {
    var division = $('.dropdown').val();
    var layer = map._layers[division];
    layer.fire("click");

  });

  if (getURLParameter('div') != null) {
    var division = getURLParameter('div').toUpperCase();
    var layer = map._layers[division];
    layer.fire("click");
  }
});

// Define OSM layer var
var OpenStreetMap_BlackAndWhite = L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
    maxZoom: 14,
    minZoom: 4,
    attribution: 'Map tiles &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

// add OpenStreetMap tile layer
OpenStreetMap_BlackAndWhite.addTo(map);

// Adds attributiokkn
map.attributionControl.addAttribution('data from ' +
  '<a href="http://www.abs.gov.au/">Australian Bureau of Statistics</a> (under a <a href="https://creativecommons.org/licenses/by/2.5/au/">CC-BY 2.5 AU license</a>). ' +
  'Developed by the <a href="https://www.griffith.edu.au/business-government/policy-innovation-hub">Policy Innovation Hub at Griffith University</a>. ' +
  '<a href="https://gitlab.com/pih/qldmarriage">See the code at GitLab</a>.'
);

// Resize on window resize
$(window).on("resize", function() {
    $("#map").height($(window).height()).width($(window).width());
    map.invalidateSize();
}).trigger("resize");

// Numbers with commas
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// return function to get div parameters
// (from https://stackoverflow.com/questions/11582512/how-to-get-url-parameters-with-javascript)
function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}
function addURLParameter(name) {
  var param = "?div=" + divName(name);
  window.history.pushState("Policy Innovation Hub: Queensland Election Map", "", param)
}

function getShareLink(name)
{
  var baseurl = location.protocol + '//' + location.host + location.pathname;
  return baseurl + "?div=" + divName(name);
}

$(window).on("load", function() {
  map.spin(false);

});
